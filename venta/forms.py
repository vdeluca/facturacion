# -*- coding: utf-8 -*-
from django import forms
from models import IVA_CHOICES
from models import Item
from models import Cliente
from models import ItemDocumento

class EncabezadoDocumentoForm(forms.Form):
    cuit = forms.CharField(max_length=13, label="CUIT")
    razon_social = forms.CharField(max_length=50, label="Razón Social")
    domicilio_comercial = forms.CharField(max_length=100, label="Domicilio Comercial")
    condicion_iva = forms.ChoiceField(choices=IVA_CHOICES, label="Condición frente al IVA")

class ItemDocumentoForm(forms.Form):
    nombre = forms.CharField(max_length=200, label="Nombre")
    precio = forms.DecimalField(max_digits=8, decimal_places=2, label="Precio de Lista")
    precio_oferta = forms.DecimalField(max_digits=8, decimal_places=2, label="Precio Oferta")

class AddItemForm(forms.ModelForm):
    class Meta:
        model = ItemDocumento
        fields = ["item", "cantidad", "descuentos", "nota"]

    # item = forms.ModelChoiceField(queryset=Item.objects.all(), label="Item")
    # cantidad = forms.IntegerField(label="Cantidad", min_value=1)
    # nota = forms.CharField(max_length=300, label="Nota", empty_value='', required=False )
    # descuento = forms.DecimalField(label="Descuento", max_value=100, min_value=0,max_digits=2,
    #                               decimal_places=1, required=False)

class ClienteForm(forms.ModelForm):

    class Meta:
        model = Cliente
        fields = ['cuit', 'razon_social', 'domicilio_comercial', 'condicion_iva', 'mail', 'telefono', 'nacionalidad']

class ItemForm(forms.ModelForm):

    class Meta:
        model = Item
        fields = ['nombre', 'precio', 'precio_oferta', 'titulo_precio_oferta']

class BuscarArticuloForm(forms.Form):
    nombre = forms.ModelChoiceField(queryset=Item.objects.all(),label="Item")

class BuscarClienteForm(forms.Form):
    cliente = forms.CharField(max_length="50")