# -*- coding: utf-8 -*-
# encoding:utf-8

from __future__ import unicode_literals
from django.contrib.auth.models import User

from django.db import models
from datetime import datetime

# CHOICES
IVA_CHOICES = (
    ('01', 'Responsable Inscripto'),
    ('02', 'Responsable no Inscripto'),
    ('03', 'No Responsable'),
    ('04', 'Exento'),
    ('05', 'Consumidor Final'),
    ('06', 'Monotributo'),
    ('07', 'No Categoriado'),
    ('08', 'Importador'),
    ('09', 'Exterior'),
    ('10', 'Liberado'),
    ('11', 'Responsable Inscripto - Agente de Percepción')
)

# Comenzado: Temporal, creado con o sin datos, debe tener
#           * cajero_emisor
#           * caja_emisora
#           * fecha_emision
#           * estado = Comenzado
ESTADO_DOCUMENTO_CHOICES = (
    ('01', 'Comenzado'),
    ('02', 'Finalizado'),
    ('03', 'Facturado')
)
# Create your models here.

class NonTrashManager(models.Manager):
    ''' Query only objects which have not been trashed. '''
    def get_query_set(self):
        query_set = super(NonTrashManager, self).get_query_set()
        return query_set.filter(trashed_at__isnull=True)


class TrashManager(models.Manager):
    ''' Query only objects which have been trashed. '''
    def get_query_set(self):
        query_set = super(TrashManager, self).get_query_set()
        return query_set.filter(trashed_at__isnull=False)


class Cliente(models.Model):
    razon_social = models.CharField(max_length=50, verbose_name="Razón Social")
    domicilio_comercial = models.CharField(max_length=100, verbose_name="Domicilio Comercial")
    condicion_iva = models.CharField(choices=IVA_CHOICES, verbose_name="Condición frente al IVA", max_length=2)
    cuit = models.CharField(max_length=13, verbose_name="CUIT")
    mail = models.CharField(max_length=50, verbose_name="e-mail")
    telefono = models.CharField(max_length=30, verbose_name=u'Teléfono')
    #Choices by pais
    nacionalidad = models.CharField(max_length=30, blank=True, null=True)

    #Variables para el restore
    #Obtenido de aca https://ltslashgt.com/2007/07/18/undelete-in-django/
    trashed_at = models.DateTimeField(blank=True, null=True)
    objects = NonTrashManager()
    trash = TrashManager()

    def delete(self, trash=True):
        if not self.trashed_at and trash:
            self.trashed_at = datetime.now()
            self.save()
        else:
            super(Cliente, self).delete()

    def restore(self, commit=True):
        self.trashed_at = None
        if commit:
            self.save()

    def __str__(self):
        if self.trashed_at:
            return '{} ({})'.format(self.razon_social, 'trashed')
        else:
            return self.razon_social

        #trashed = (self.trashed_at and 'trashed' or 'not trashed')
        #return '%s (%s)' % (self.razon_social, trashed)
        #return self.razon_social


class PuntoVenta(models.Model):
    numero = models.PositiveIntegerField(verbose_name="Número", unique=True)
    nombre = models.CharField(max_length=60, verbose_name="Nombre")
    crt = models.FileField(upload_to="certificados/", null=False, blank=False, verbose_name="CRT")
    key = models.FileField(upload_to="certificados/", null=False, blank=False, verbose_name="Private Key")
    razon_social = models.CharField(max_length=50, verbose_name="Razón Social")
    domicilio_comercial = models.CharField(max_length=100, verbose_name="Domicilio Comercial")
    condicion_iva = models.CharField(choices=IVA_CHOICES, verbose_name="Condición frente al IVA", max_length=2)
    cuit = models.CharField(max_length=13, verbose_name="CUIT")
    ingresos_brutos = models.CharField(max_length=20, verbose_name="Ingresos Brutos")
    inicio_actividades = models.CharField(max_length=15, verbose_name="Fecha Inicio Actividades")


    def __str__(self):
        return str(self.numero)


class Caja(models.Model):
    puntoVenta = models.ForeignKey(PuntoVenta, null=False, blank=False, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=60, verbose_name="Definición")

    def __str__(self):
        return self.nombre


class Cajero(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    caja = models.ForeignKey(Caja, null=False, blank=False, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=70, null=False, blank=False)
    es_jefe_de_caja = models.BooleanField(verbose_name="Es Jefe de caja")
    es_administrador = models.BooleanField(verbose_name="Es Administrador general")

    def getCajeroByUser(cls, self, userid):
        usuario = User.objects.get(pk=userid)
        cajero = Cajero.objects.get(user=usuario)

        return cajero

    def __str__(self):
        return self.nombre


class TipoPago(models.Model):
    nombre = models.CharField(max_length=200, verbose_name="Definición")

    class Meta:
        verbose_name = "Tipo de Pago"
        verbose_name_plural = "Tipos de Pago"

    def __str__(self):
        return self.nombre


class Descuento(models.Model):
    nombre = models.CharField(max_length=200, verbose_name="Definición")
    porcentaje = models.DecimalField(max_digits=4, decimal_places=2)
    monto_fijo = models.IntegerField(verbose_name="Monto Fijo", blank=True, null=True)

    class Meta:
        verbose_name = "Descuento"
        verbose_name_plural = "Descuentos"

    def __str__(self):
        return self.nombre


class Item(models.Model):
    nombre = models.CharField(max_length=200, verbose_name="Nombre", blank=False, null=False)
    precio = models.DecimalField(max_digits=8, decimal_places=2, verbose_name="Precio de Lista", null=False,
                                 blank=False)
    precio_oferta = models.DecimalField(max_digits=8, decimal_places=2, verbose_name="Precio Oferta", null=True,
                                        blank=True)
    titulo_precio_oferta = models.CharField(max_length=80, null=True, blank=True)

    #Variables para el restore
    #Obtenido de aca https://ltslashgt.com/2007/07/18/undelete-in-django/
    trashed_at = models.DateTimeField(blank=True, null=True)
    objects = NonTrashManager()
    trash = TrashManager()

    def delete(self, trash=True):
        if not self.trashed_at and trash:
            self.trashed_at = datetime.now()
            self.save()
        else:
            super(Item, self).delete()

    def restore(self, commit=True):
        self.trashed_at = None
        if commit:
            self.save()

    def getPrecio(self):
        if self.precio_oferta > 0:
            return "* {} ({})".format(self.precio_oferta, self.titulo_precio_oferta)
        else:
            return self.precio

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = "Producto o Servicio"
        verbose_name_plural = "Productos o Servicios"
        ordering = ("nombre",)

###
# DOS TIPOS DE DOCUEMNTOS: 1) RESERVAS, 2) VENTAS
# LAS VENTAS PUEDEN SER BLANCAS O NEGRAS. LAS BLANCAS TIENEN CAE. AL FACTURAR UNA VENTA SE EDITA EL REGISTRO DE VENTA
# EL CAMPO documento_relacionado CONTIENE LA RESERVA PARA CADA VENTA
class Documento(models.Model):
    cae = models.CharField(max_length=14, verbose_name="CAE", null=True, blank=True, default=None)
    pdf = models.FileField(upload_to="documentos/", null=True, blank=True, verbose_name="Archivo PDF")
    cajero_emisor = models.ForeignKey(Cajero, blank=False, null=False, on_delete=models.CASCADE)
    caja_emisora = models.ForeignKey(Caja, blank=False, null=False, on_delete=models.CASCADE)
    fecha_emision = models.DateTimeField(verbose_name="Fecha/Hora de emisión", auto_now_add=True)
    pago = models.ForeignKey(TipoPago, blank=False, null=True, on_delete=models.CASCADE)
    descuentos = models.ManyToManyField(Descuento, blank=True, null=True)
    documento_relacionado = models.ManyToManyField('self', verbose_name="Documento Relacionado", blank=True)
    estado = models.CharField(max_length=2, choices=ESTADO_DOCUMENTO_CHOICES)
    #Datos comprador
    #Campo relacional opcional por si decide guardar el cliente luego de realizar la factura, su función es la de autocomplete
    rel_cliente = models.ForeignKey(Cliente, verbose_name="Cliente", blank=True, null=True)
    razon_social = models.CharField(max_length=50, verbose_name="Razón Social", blank=True, null=True)
    domicilio_comercial = models.CharField(max_length=100, verbose_name="Domicilio Comercial", blank=True, null=True)
    condicion_iva = models.CharField(choices=IVA_CHOICES, verbose_name="Condición frente al IVA", max_length=2,
                                     blank=True, null=True)
    cuit = models.CharField(max_length=13, verbose_name="CUIT",
                            blank=True, null=True)

    ############# Totales #############
    # Total sumado de los items
    total_bruto = models.DecimalField(verbose_name="Total Bruto", max_digits=8, decimal_places=2,
                                      blank=True, null=True)
    # total_bruto con impuestos
    total_neto = models.DecimalField(verbose_name="Total", max_digits=8, decimal_places=2,
                                     blank=True, null=True)
    # total_bruto menos descuentos
    total = models.DecimalField(verbose_name="Total", max_digits=8, decimal_places=2,
                                blank=True, null=True)

    def __str__(self):
        return str(self.fecha_emision)

    # get documento OBJ
    # No pude devolver un objeto Documento   ## Frustrado  L:( ##
    def newTmpDocument(self, id):
        cajero = Cajero()
        self.cajero_emisor = Cajero.getCajeroByUser(cajero, id)
        return self

    def setCurrentConsFinal(self, request):
        self.cajero_emisor = Cajero.get(user = User.get(pkid=request.User.id))
        return self

class ItemDocumento(models.Model):
    documento = models.ForeignKey(Documento, on_delete=models.CASCADE)
    nota = models.CharField(max_length=20, blank=True)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    descuentos = models.ForeignKey(Descuento, blank=True, verbose_name="Descuentos", null=True)
    cantidad = models.IntegerField(default=0)
    precio = models.DecimalField(max_digits=8, decimal_places=2, verbose_name="Precio de Lista", null=False,
                                 blank=False, default='0')

    def __str__(self):
        return "{}".format(self.item)