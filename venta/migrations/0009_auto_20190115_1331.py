# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2019-01-15 13:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('venta', '0008_auto_20190107_2325'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documento',
            name='descuentos',
            field=models.ManyToManyField(to='venta.Descuento'),
        ),
        migrations.AlterField(
            model_name='documento',
            name='documento_relacionado',
            field=models.ManyToManyField(blank=True, related_name='_documento_documento_relacionado_+', to='venta.Documento', verbose_name='Documento Relacionado'),
        ),
        migrations.AlterField(
            model_name='itemdocumento',
            name='descuentos',
            field=models.ManyToManyField(blank=True, to='venta.Descuento', verbose_name='Descuentos'),
        ),
    ]
