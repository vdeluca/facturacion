# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2019-04-23 23:22
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('venta', '0020_itemdocumento_cantidad'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='itemdocumento',
            name='descuentos',
        ),
        migrations.AddField(
            model_name='itemdocumento',
            name='descuentos',
            field=models.ForeignKey(blank=True, default='', on_delete=django.db.models.deletion.CASCADE, to='venta.Descuento', verbose_name='Descuentos'),
            preserve_default=False,
        ),
    ]
