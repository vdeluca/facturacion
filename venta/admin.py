# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import *

# Register your models here.
# ------------------------------------------
# REGITRACIONES DIRECTAS
# ------------------------------------------
admin.site.register(Caja)
admin.site.register(Cajero)
admin.site.register(Item)
#admin.site.register(Documento)
admin.site.register(ItemDocumento)
admin.site.register(Cliente)


class PuntoVentaAdmin(admin.ModelAdmin):
    list_display = (["numero", "nombre"])

admin.site.register(PuntoVenta, PuntoVentaAdmin)

class DescuentoAdmin(admin.ModelAdmin):
    list_display = (["nombre", "porcentaje"])

admin.site.register(Descuento,DescuentoAdmin)


class TipoPagoAdmin(admin.ModelAdmin):
    list_display = (["nombre"])

admin.site.register(TipoPago,TipoPagoAdmin)

# Documentos
class ItemInline(admin.TabularInline):
    model = ItemDocumento

class DocumentoAdmin(admin.ModelAdmin):
    readonly_fields = ('estado', 'fecha_emision',)
    fields = (('cajero_emisor', 'caja_emisora'), 'pago', 'descuentos', 'total_bruto', 'total_neto', 'total')
    list_display = ('cajero_emisor', 'caja_emisora', 'rel_cliente', 'total_bruto', 'total_neto', 'total')
    list_filter = ('estado', )
    inlines = [
        ItemInline
    ]


admin.site.register(Documento,DocumentoAdmin)
