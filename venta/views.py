# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponseRedirect
from .forms import EncabezadoDocumentoForm
from django.shortcuts import render
import datetime
from models import Documento, Item, Cliente, Cajero, Caja, ESTADO_DOCUMENTO_CHOICES, ItemDocumento
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from .forms import AddItemForm, BuscarClienteForm
from .forms import ClienteForm
from .forms import ItemForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect

def index(request):
    return render(request, 'index.html', {})

@login_required
def documentos(request):
    docs = Documento.objects.all().order_by('-fecha_emision')
    context = {"documentos": docs}
    return render(request, 'listado_documentos.html', context)

@login_required
def documento(request):
    html_page = "encabezado_documento.html"
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        if request.POST["step"] == 'step1':
            form = ItemDocumentoForm(request.POST)
            html_page = "grilla_items.html"
        else:
            form = EncabezadoDocumentoForm(request.POST)
            html_page = "encabezado_documento.html"

        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponseRedirect('/thanks/')
            # if a GET (or any other method) we'll create a blank form
    else:
        form = EncabezadoDocumentoForm()

    return render(request, html_page, {'form': form})

@login_required
def nuevodocumento_paso1(request):
    clienteForm = BuscarClienteForm()
    context = {'form': clienteForm}
    return render(request,'encabezado_documento.html',context)

"""Return templates/agregar_items_factura.html

"""
@login_required
def agregaritems(request):
    user = request.user
    cajero =  get_object_or_404(Cajero, user=user)
    caja = cajero.caja
    doc, created = Documento.objects.get_or_create(cajero_emisor= cajero,
                                        caja_emisora=caja,
                                        estado='01')
    doc.save()
    form = AddItemForm()
    items = ItemDocumento.objects.all().filter(documento=doc)
    context = {"documento": doc, "formAddItem": form, "items": items}
    return render(request,"agregar_items_factura.html",context)

"""Return templates/agregar_items_factura.html

"""
@login_required
def agregaritem(request, documento):
    form = AddItemForm(request.POST)
    if form.is_valid() and request.method == 'POST':
        item = get_object_or_404(Item, pk=form["item"].value())
        doc = get_object_or_404(Documento, pk=documento)
        item_doc, created = ItemDocumento.objects.get_or_create(documento=doc, item=item)
        item_doc.cantidad = item_doc.cantidad + int(form["cantidad"].value())
        item_doc.precio = item.precio
        item_doc.save()
    # response
    resp = redirect("/agregar-items/")
    return resp

@login_required
def continuardocumento(request):
    pass

@login_required
def items(request):
    items = Item.objects.all()
    paginator = Paginator(items,15)
    page = request.GET.get('page')
    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        items = paginator.page(1)
    except EmptyPage:
        items = paginator.page(paginator.num_pages)

    context = {"items": items}
    return render(request,'listado_items.html',context)

@login_required
def nuevoitem(request):
    if request.POST.get('doadd')=="1":
        form = ItemForm(request.POST)
        if form.is_valid():
            form.save()
            response = redirect("/items/")
            return response
        else:
            error = form.errors
            context = {'form': form, "error": error}
            return render(request, 'nuevo_item.html', context)
    else:
        form = ItemForm()
        context = {'form': form}
        return render(request,'nuevo_item.html', context)

@login_required
def editaritem(request):
    if request.POST.get('doedit')=="1":
        item = get_object_or_404(Item, pk=request.GET.get('id'))
        form = ItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            response = redirect('/items/')
            return response
        else:
            error = form.errors
            context = {'form': form, "error": error}
            return render(request, "editar_item.html",context)
    else:
        item = get_object_or_404(Item, pk=request.GET.get('id'))
        form = ItemForm(instance=item)
        context = {'form': form,"item": item}
        return render(request, 'editar_item.html', context)

@login_required
def eliminaritem(request):
    item = get_object_or_404(Item, pk=request.GET.get('id'))
    if item.id:
        item.delete()
        message = "Se ha eliminado al item {}".format(item.nombre)
    else:
        message = "No se ha encontrado ningún item con el id={}".format(request.GET.get('id'))

    response = redirect('/items')
    return response

def documentos(request):
    docs = Documento.objects.all().order_by('-fecha_emision')
    context = {"documentos": docs}
    return render(request, 'listado_documentos.html', context)

def servicios(request):
    servicios = Item.objects.all().order_by('-id')
    context = {"servicios": servicios}
    return render(request, 'listado_servicios.html', context)

def clientes(request):
    clientes = Cliente.objects.all().filter(trashed_at=None)
    context = {"clientes": clientes}
    return render(request, 'listado_clientes.html', context)

def editarcliente(request):
    cli = Cliente.objects.get(id=request.GET.get('id'))
    clientes = Cliente.objects.all().filter(trashed_at=None).order_by('-id')
    if cli.id:
        message = "Se va a editar el cliente {}".format(cli.razon_social)
    else:
        message = "No se ha encontrado ningún cliente con el id={}".format(request.GET.get('id'))

    form = ClienteForm(instance=cli)

    context={'mensaje':message, "clientes": clientes, 'form':form, 'cliente': cli}
    return render(request,'editar_cliente.html', context)

def doeditcliente(request):
    cli = Cliente.objects.get(id=request.GET.get('id'))
    form = ClienteForm(request.POST, instance=cli)
    if form.is_valid():
        form.save()
    else:
        render(request,'editar_cliente.html')

    message = "Se editó el cliente {}".format(form.data['razon_social'])
    clientes = Cliente.objects.all()
    context = {'mensaje':message, "clientes":clientes}
    return render(request,'listado_clientes.html',context)

def eliminarcliente(request):
    cli = Cliente.objects.get(id=request.GET.get('id'))
    clientes = Cliente.objects.all().filter(trashed_at=None).order_by('-id')
    if cli.id:
        cli.delete()
        message = "Se ha eliminado al cliente {}".format(cli.razon_social)
        clitrash = cli.id
    else:
        message = "No se ha encontrado ningún cliente con el id={}".format(request.GET.get('id'))

    context = {'mensaje': message, "clientes": clientes, "clitrash": clitrash}
    return render(request, 'listado_clientes.html', context)

def restorecliente(request):
    cli = Cliente.objects.get(id=request.GET.get('idclitrash'))
    cli.restore()
    clientes = Cliente.objects.all().filter(trashed_at=None).order_by('-id')
    message = "Se ha recuperado al cliente {}".format(cli.razon_social)
    context = {'mensaje': message, "clientes": clientes}
    return render(request, 'listado_clientes.html', context)

#Continuar documento
#Cuando un documento tiene el encabezado pero aún se pueden agregar artículos
def continuardoc(request, doc_id):
    doc = get_object_or_404(Documento, pk=doc_id)
    listItems = ItemDocumento.objects.filter(documento=doc)
    context = {"doc": doc, "items": listItems}
    return render(request, 'agregar_items_factura.html', context)

# Crea un documento y establece
#
def crearDocCF(request):
    cajero = Cajero.objects.get(user=request.user)

    doc = Documento()
