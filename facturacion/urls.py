"""facturacion URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from venta import views as venta_views

#   Nota para documento y nuevodocumento
# Crea un nuevo documento como nuevodocumento,
# Pero tiene otro form porque agrega campos,
# Como nacionalidad, los ultimos.
# Ademas agrega un default en IVA

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', venta_views.index, name="index"),
    url(r'^documentos/$', venta_views.documentos, name="documentos"),
    url(r'^nuevo-documento/$', venta_views.nuevodocumento_paso1, name="nuevo-documento"),
    url(r'^agregar-items/$', venta_views.agregaritems, name="agregar-items"),
    url(r'^agregar-item/(?P<documento>\d+)/$', venta_views.agregaritem, name="agregar-item"),

    url(r'^documento/$', venta_views.documento, name="documento"),
    url(r'^nuevodocumentos/$', venta_views.nuevodocumento_paso1, name="nuevodocumentos"),
    url(r'^servicios/$', venta_views.servicios, name="servicios"),
    url(r'^items/$', venta_views.items, name="items"),
    url(r'^nuevoitem/$', venta_views.nuevoitem, name="nuevoitem"),
    url(r'^eliminaritem/$', venta_views.eliminaritem, name="eliminaritem"),
    url(r'^editaritem/$', venta_views.editaritem, name="editaritem"),
    url('^accounts/', admin.site.urls),
    url('^clientes/', venta_views.clientes, name="clientes"),
    url('^editarcliente/$', venta_views.editarcliente, name="editarcliente"),
    url('^doeditcliente/$', venta_views.doeditcliente, name="doeditcliente"),
    url('^restorecli/$', venta_views.restorecliente, name="restorecliente"),
    url('^eliminarcliente/$', venta_views.eliminarcliente, name="eliminarcliente"),
    url(r'^continuardoc/(?P<doc_id>\d+)/$', venta_views.continuardoc, name="continuardoc"),
]
